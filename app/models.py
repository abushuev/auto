from app import db
from sqlalchemy import Column, Integer, String, DateTime
import datetime

class AutoFuelDb(db.Model):
    __tablename__ = 'auto_fuel'
    id        = Column(Integer, primary_key=True)
    timestamp = Column(Integer, index=True)
    location  = Column(String)
    vendor    = Column(String)
    volume_fp = Column(Integer)
    total_fp  = Column(Integer)
    odometer  = Column(Integer)
    type      = Column(Integer)

    def to_fp(self,ceil,frag):
        return (ceil*100) + (frag%100)
    
    def from_fp(self,fp):
        #ceil = fp / 100
        #frag = fp % 100
        return '%i,%02i' % (fp/100,fp%100)

    def get_datetime(self):
        return datetime.datetime.fromtimestamp(self.timestamp).strftime('%d.%m.%Y %H:%M')

    def get_volume(self):
        return self.from_fp(self.volume_fp)
    
    def get_price(self):
        return self.from_fp((self.total_fp * 100 + 99) / self.volume_fp)
    
    def get_total(self):
        return self.from_fp(self.total_fp)

    # time.mktime(time.strptime('2000-01-01 10:00', '%Y-%m-%d %H:%M'))
    # time.mktime(time.strptime('01.01.2000 10:00', '%d.%m.%Y %H:%M'))
    # datetime.datetime.fromtimestamp(946710060).strftime('%Y-%m-%d %H:%M')

    def __repr__(self):
        return '<AutoFuel: %r %r>' % (self.location, self.volume)
