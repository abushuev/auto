# -*- coding: utf-8 -*-
from app import app
from locale import loc
from flask import render_template, jsonify, g, request
from models import AutoFuelDb
import json

class Object(): pass

class cAuto():
    def json_fuel(self):
        records = AutoFuelDb.query.order_by(AutoFuelDb.timestamp.desc()).all()
    
        lines=[]
        for rec in records:
            line={}
            line['Date and time'] = rec.get_datetime()
            line['Location']      = rec.location
            line['Vendor']        = rec.vendor
            line['Volume']        = rec.get_volume()
            line['Price']         = rec.get_price()
            line['Total']         = rec.from_fp(rec.total_fp)
            line['Odometer']      = rec.odometer
            line['Comment']       = rec.type
            lines.append(line)
        
        ret = json.dumps(lines,sort_keys=True)
        return ret#, 200, {'Content-Type': 'application/json; charset=utf-8'}

    def web_fuel(self):
        return render_template('app.fuel.html',loc=loc)
    
Auto=cAuto()

@app.route('/json/show')
def json_show():
    url=request.query_string
    return render_template('json.show.html',
                           url=url)

@app.route('/auto/json/fuel')
def auto_json_fuel():
    return Auto.json_fuel()

@app.route('/')
@app.route('/index')
@app.route('/auto/fuel')
def auto_fuel():
    return Auto.web_fuel()
