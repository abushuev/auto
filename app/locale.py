# -*- encoding: utf-8 -*-

locale_ru = {
    'New'           : u'Новый',
    'Fuel'          : u'Бензин',
    'Parts'         : u'Запчасти',
    'Works'         : u'Работы',
    'Plans'         : u'Планы',
    'Report'        : u'Отчёт',
    'Date and time' : u'Дата и время',
    'Location'      : u'Место',
    'Vendor'        : u'Заправка',
    'Volume'        : u'Литров',
    'Price'         : u'Цена',
    'Total'         : u'Итого',
    'Odometer'      : u'Одометр',
    'Distance'      : u'Пробег',
    'Discharge'     : u'Расход',
    'Comment'       : u'Комментарий',
    'clear'         : u'сброс',
    'non-full'      : u'неполный',
    'full'          : u'полный'
}

locale_en = {}

from config import LOCALE
def loc(word):
    if LOCALE=='ru':
        locale = locale_ru
    else:
        locale = locale_en
    
    if word in locale:
        return locale[word]
    return word
